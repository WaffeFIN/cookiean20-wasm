import { Interpreter } from "cookiean-wasm";

var interpreter = new Interpreter();

// Hook console.error for Rust panics
console.stderr = console.error.bind(console);
console.errors = [];
console.error = function(){
    console.errors.push(Array.from(arguments));
    console.stderr.apply(console, arguments);
}

const codeStorage = window.localStorage;
{
	const cookieanCode = codeStorage.getItem("code");
	code.textContent = cookieanCode ? cookieanCode : "\"Hello world!\" displayed";
}

const getCode = () => {
	return code.value;
}

const saveCode = () => {
	const cookieanCode = getCode();
	if (cookieanCode.trim()) {
		codeStorage.setItem("code", cookieanCode);
	}
}

setInterval(saveCode, 60 * 1000);

const LABEL_RUN_CODE = "Run code\n▶";
const LABEL_RUNNING  = "Running...\n■";

run.textContent = LABEL_RUN_CODE;

const Run = () => {
	// Clear output
	output.textContent = "";
	// Run code
	var code = getCode();
	var error_message = "";
	
	try {
		error_message = interpreter.interpret(code);
	} catch (_unreachable) {
		error_message = "\nRuntime error:\n"
		error_message += console.errors[console.errors.length - 1].toString()
			// Remove unhelpful stack trace
			.split("Stack:\n")[0];
		interpreter = new Interpreter(); // Reload
	}
	output.classList[error_message ? "add" : "remove"]('wrong');
	output.textContent += error_message;
	run.textContent = LABEL_RUN_CODE;
}


run.addEventListener("click", event => {
	if (run.textContent === LABEL_RUN_CODE) {
		saveCode();
		run.textContent = LABEL_RUNNING;
		setTimeout(Run, 0); // Refresh label before running
	} else if (run.textContent === LABEL_RUNNING) { // TODO asyncify the Run to allow this "interruption"
		saveCode();
		location.reload();
	}
});

input.addEventListener('keydown', event => {
    if (event.key === 'Enter' || event.keyCode === 13) {
        AddInput(); // TODO
    }
});


// Code checking
var checkHadError = false;

const Check = () => {
	// Check code
	var code = getCode();
	var error_message = "";
	
	try {
		error_message = interpreter.check(code);
	} catch (_unreachable) {
		error_message = "\nWaffe writes bad code:\n"
		error_message += console.errors[console.errors.length - 1].toString()
			// Remove unhelpful stack trace
			.split("Stack:\n")[0];
		interpreter = new Interpreter(); // Reload
	}
	output.classList[error_message ? "add" : "remove"]('wrong');
	if (error_message) {
		output.textContent = error_message;
		checkHadError = true;
	} else if (checkHadError) {
		output.textContent = "";
		checkHadError = false;
	}
}

const calculateDelay = (byte) => {
	return byte * byte;
}

var syntaxCheckTimeout;

code.addEventListener('keydown', event => {
	if (syntaxCheckTimeout)
		window.clearTimeout(syntaxCheckTimeout);
	
	if (ms.textContent.endsWith("ms")) {
		const delay = +(ms.textContent.split(' ')[0]);
		syntaxCheckTimeout = setTimeout(Check, delay);
	} else if (ms.textContent.endsWith("s")) {
		const delay = +(ms.textContent.split(' ')[0]) * 1000;
		syntaxCheckTimeout = setTimeout(Check, delay);
	}
});

check.addEventListener('change', event => {
	if (event.target.value > 255) {
		ms.textContent = "Never";
	} else {
		const delay = calculateDelay(event.target.value);
		if (delay >= 1000) {
			ms.textContent = (delay / 1000).toFixed(2) + " s"; 
		} else {
			ms.textContent = delay + " ms";
		}
	}
});

// Update values
check.dispatchEvent(new Event("change"));
mod utils;

extern crate baker;

use wasm_bindgen::prelude::{ wasm_bindgen };

// When the `wee_alloc` feature is enabled, use `wee_alloc` as the global
// allocator.
#[cfg(feature = "wee_alloc")]
#[global_allocator]
static ALLOC: wee_alloc::WeeAlloc = wee_alloc::WeeAlloc::INIT;

#[wasm_bindgen]
pub struct Interpreter {
	input: SimpleBufRead,
	output: SimpleWrite
}

#[wasm_bindgen]
impl Interpreter {
    #[wasm_bindgen(constructor)]
    pub fn new() -> Interpreter {
		utils::set_panic_hook();
        Interpreter {
			input: SimpleBufRead { buffer: Vec::new() },
			output: SimpleWrite { }
		}
	}

	pub fn check(&self, mut code: String) -> String {
		code.push('\n');
		let code = &code[..];
		let (tokens, lexer_errors) = baker::tokenizing::lexer::Lexer::tokenize_cookiean(code);
		let (rows, row_parser_errors) = baker::parsing::row_parser::parse(tokens);
		let (_, block_parser_errors) = baker::parsing::block_parser::parse(rows);
	
		let (msg, n_errs) = baker::tokenizing::structs::stringify_errors(vec![block_parser_errors, row_parser_errors, lexer_errors]);
	
		if n_errs > 0 {
			return format!("{}\n\nFound {} error(s)", msg, n_errs);
		}
		return String::default();
	}

	pub fn interpret(&mut self, mut code: String) -> String {
		code.push('\n');
		let code = &code[..];
		let (tokens, lexer_errors) = baker::tokenizing::lexer::Lexer::tokenize_cookiean(code);
		let (rows, row_parser_errors) = baker::parsing::row_parser::parse(tokens);
		let (ast_root, block_parser_errors) = baker::parsing::block_parser::parse(rows);
	
		let (msg, n_errs) = baker::tokenizing::structs::stringify_errors(vec![block_parser_errors, row_parser_errors, lexer_errors]);
	
		if n_errs > 0 {
			return format!("{}\n\nFound {} error(s)", msg, n_errs);
		}
		
		let instructions = Vec::from(ast_root);
		baker::interpreting::crumbs_interpreter::run(instructions, &mut self.input, &mut self.output, false);
		return String::default();
	}
}

// TODO BufRead and Write wrappers for Input and TextArea
struct SimpleBufRead {
	buffer: Vec<u8>
}
impl std::io::Read for SimpleBufRead {
	fn read(&mut self, buf: &mut [u8]) -> std::io::Result<usize> {
		todo!() // html Input text with as_bytes().read(buf)
	}
}
impl std::io::BufRead for SimpleBufRead {
    fn fill_buf(&mut self) -> std::io::Result<&[u8]> {
		todo!() // copy html Input text with as_bytes 
	}
    fn consume(&mut self, amt: usize) {
		todo!() // remove this amount of bytes from html Input text
	}
}

struct SimpleWrite {}
impl std::io::Write for SimpleWrite {
    fn write(&mut self, buf: &[u8]) -> std::io::Result<usize> {
		let window = web_sys::window().expect("no global `window` exists");
		let document = window.document().expect("should have a document on window");
		let output = document.get_element_by_id("output").expect("could not find element with id 'output'");
	
		let new_text = std::str::from_utf8(buf).expect("buffer not valid UTF-8").clone();
		let previous_text = output.text_content().unwrap_or(String::default());
		output.set_text_content(Some(&*format!("{}{}", previous_text, new_text)));

		// todo limit text content length
		
		Ok(buf.len())
	}

    fn flush(&mut self) -> std::io::Result<()> {
		Ok(())
	}
}